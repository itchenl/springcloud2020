package com.chenl.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author:chenl
 * @Date:2020/10/22 16:40
 * @Version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
public class PaymentMain8001 {
    public static void main(String[] args) {

        //我是本地新加的
        SpringApplication.run(PaymentMain8001.class,args);
    }
}
