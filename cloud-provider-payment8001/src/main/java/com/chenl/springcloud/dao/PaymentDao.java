package com.chenl.springcloud.dao;

import com.chenl.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author:chenl
 * @Date:2020/10/22 17:08
 * @Version 1.0
 */
@Mapper
public interface PaymentDao {

    public  int create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
