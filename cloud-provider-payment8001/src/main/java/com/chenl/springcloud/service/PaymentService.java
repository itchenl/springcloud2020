package com.chenl.springcloud.service;

import com.chenl.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @Author:chenl
 * @Date:2020/10/22 17:53
 * @Version 1.0
 */
public interface PaymentService {

    public  int create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
