package com.chenl.springcloud.service.impl;

import com.chenl.springcloud.dao.PaymentDao;
import com.chenl.springcloud.entities.Payment;
import com.chenl.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author:chenl
 * @Date:2020/10/22 17:54
 * @Version 1.0
 */
@Service
public class PaymentImpl implements PaymentService{

    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }
    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
